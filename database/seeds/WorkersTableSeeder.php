<?php

use Illuminate\Database\Seeder;

class WorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 50; $i++) {
            App\Models\Workers::create([
                'name' => $faker->name,
                'surname' => $faker->userName,
                'for_an_hour' => $faker->numberBetween(1,100),
                'street' => $faker->streetName,
                'city' => $faker->city,
                'post_code' => str_random(6)
            ]);
        }
    }
}
