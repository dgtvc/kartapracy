<?php

use Faker\Generator as Faker;

$factory->define(App\Workers::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->userName,
        'for_an_hour' => $faker->numberBetween(1,100),
        'street' => $faker->streetName,
        'city' => $faker->city,
        'post_code' => $faker->postcode
    ];
});
