<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','contract_name','manager_id'];

    public function managers()
    {
        return $this->belongsToMany('App\Models\Manager','buildings_manager','building_id', 'manager_id');
    }

//    public function managers()
//    {
//        return $this->hasMany('App\Models\Manager','building_id','id');
//    }
//
//    public function manager()
//    {
//        return $this->belongsTo('App\Models\Manager','manager_id','id');
//    }
}
