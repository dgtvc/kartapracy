<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkersWorkChanges extends Model
{

    protected $fillable = ['user_id','workers_work_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

}
