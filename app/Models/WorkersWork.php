<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkersWork extends Model
{
    use SoftDeletes;

    public function building()
    {
        return $this->belongsTo('App\Models\Building','building_id','id');
    }

    public function buildingg()
    {
        return $this->belongsTo('App\Models\Building','building_id','id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Workers','worker_id','id');
    }
    public function userr()
    {
        return $this->belongsTo('App\Models\Workers','worker_id','id')->withTrashed();
    }

    public function manager()
    {
        return $this->belongsTo('App\Models\Manager','manager_id','id');
    }

    public function managerr()
    {
        return $this->belongsTo('App\Models\Manager','manager_id','id')->withTrashed();
    }

    public function changes()
    {
        return $this->hasMany('App\Models\WorkersWorkChanges', 'workers_work_id','id');
    }
}
