<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workers extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','surname','for_an_hour','street','city','post_code','state','company_id'];


    public function works()
    {
        return $this->hasMany('App\Models\WorkersWork','worker_id','id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id','id');
    }
}
