<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manager extends Model
{
    protected $fillable = ['name','surname','building_id','for_an_hour','street','city','post_code','state'];

    use SoftDeletes;
    
    protected $dates = ['deleted_at'];

    public function works()
    {
        return $this->hasOne('App\Models\WorkersWork','manager_id','id');
    }

    public function building()
    {
        return $this->belongsTo('App\Models\Building','building_id','id');
    }

    public function mpk_own()
    {
        return $this->HasOne('App\Models\Building','manager_id','id');
    }
}
