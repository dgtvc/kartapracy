<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BM extends Model
{

    protected $table = 'buildings_manager';

    protected $fillable = ['id','manager_id','building_id'];

    public $timestamps = false;

}
