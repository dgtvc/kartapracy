<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];

    public function workers()
    {
        return $this->hasMany('App\Models\Workers','company_id', 'id');
    }

}
