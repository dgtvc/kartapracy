<?php

namespace App\Export;

use App\Models\Workers;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * Class ReportExport
 * @package App\Export
 */
class ReportExport implements FromView{

    /**
     * @var
     */
    private $date_from;
    /**
     * @var
     */
    private $date_to;
    /**
     * @var Request
     */
    private $request;

    /**
     * ReportExport constructor.
     * @param $date_from
     * @param $date_to
     * @param Request $request
     */
    public function __construct($date_from, $date_to, Request $request)
    {
        $this->date_from = $date_from;
        $this->date_to = $date_to;
        $this->request = $request;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $date_from = $this->date_from;
        $date_to = $this->date_to;

        $user_list = Workers::has('works')->get();

        return view('excel.extend',compact('user_list','date_from','date_to'),['request' => $this->request]);
    }

}