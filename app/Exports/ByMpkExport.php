<?php

namespace App\Exports;

use App\Models\Workers;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;

/**
 * Class ByMpkExport
 * @package App\Export
 */
class ByMpkExport implements FromView
{


    /**
     * @var
     */
    private $year;
    /**
     * @var
     */
    private $month;
    /**
     * @var Request
     */
    private $request;

    /**
     * ReportExport constructor.
     * @param $year
     * @param $month
     * @param Request $request
     */
    public function __construct($year, $month, Request $request)
    {
        $this->year = $year;
        $this->month = $month;
        $this->request = $request;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $year = $this->year;
        $month = $this->month;

        $user_list = Workers::has('works')->get();

        return view('excel.mpk',compact('user_list','year','month'),['request' => $this->request]);
    }

}