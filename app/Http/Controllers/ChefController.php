<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Workers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ChefController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:manage.chef');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manage() {
        return view('pages.chef');
    }

    public function showPermissions() {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('pages.permission', compact('roles','permissions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');

        /*$search_data = [];

        if($search && $search != "") {
            $search = explode(' ', $search);
            foreach ($search as $search_item) {
                $search_data[] = ['name', 'LIKE', '%'.($search_item).'%'];
                $search_data[] = ['surname', 'LIKE', '%'.($search_item).'%'];
            }
        }*/
        if($search && $search != "") {
            $search = explode(' ', $search);
            $items = User::where(function ($q) use ($search) {
                foreach ($search as $value) {
                    $q->orWhere('name', 'like', "%{$value}%");
                    $q->orWhere('email', 'like', "%{$value}%");
                }
            })->with('roles')->paginate(20);
        } else
            $items = User::with('roles')->latest()->paginate(20);

        $user = Auth::user();

        $roles = $user->roles;
        if($roles->count())
            $role = $roles[0]->role_rank;
        else
            $role = 1;

        $roles = Role::where('role_rank','<=', $role)->get();

        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items,
            'roles' => $roles,
            'role' => $role
        ];


        return response()->json($response);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'roles' => 'required|integer'
        ]);

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $create = User::create($input);

        $create->syncRoles([$input['roles']]);

        return response()->json($create);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $edit = User::find($id);

        $edit->syncRoles([$request->roles]);

        $edit->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return response()->json(['done']);
    }
}
