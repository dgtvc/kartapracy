<?php

namespace App\Http\Controllers;

use App\Models\Manager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManagerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:manage.time')->only('time');
        $this->middleware('permission:manage.workers')->except('time');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manage() {
        return view('pages.manager');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $nolimit = $request->exists('no_limit');

        $order = $request->get('order');
        $order_direction = $request->get('order_direction');

        /*$search_data = [];

        if($search && $search != "") {
            $search = explode(' ', $search);
            foreach ($search as $search_item) {
                $search_data[] = ['name', 'LIKE', '%'.($search_item).'%'];
                $search_data[] = ['surname', 'LIKE', '%'.($search_item).'%'];
            }
        }*/


        $items = Manager::where('id','>','0');

        if($order)
            $items->orderBy($order, $order_direction);

        if($search) {
            $search = explode(' ', $search);
            $items->where(function ($q) use ($search) {
                foreach ($search as $value) {
                    $q->orWhere('name', 'like', "%{$value}%");
                    $q->orWhere('surname', 'like', "%{$value}%");
                }
            });
        }

        if($nolimit)
            return response()->json($items->get());

        $items = $items->paginate(20);

        /*if(!Auth::user()->can('manage.chef')) {
            $data = $items->getCollection();
            $data->each(function ($item) {
                $item->setHidden(['for_an_hour']); //->setVisible(['for_an_hour']);
            });
            $items->setCollection($data);
        }*/

        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items
        ];  


        return response()->json($response);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required'
        ]);

        $create = Manager::create($request->all());

        return response()->json($create);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Manager  $workers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required'
        ]);

        $edit = Manager::find($id)->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Manager::find($id)->delete();
        return response()->json(['done']);
    }
}
