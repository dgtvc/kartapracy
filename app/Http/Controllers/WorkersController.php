<?php

namespace App\Http\Controllers;

use App\Models\Workers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WorkersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:manage.time')->only('time');
        $this->middleware('permission:manage.workers')->except('time');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manage() {
        return view('pages.workers');
    }


    public function time() {
        return view('pages.time');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $order = $request->get('order');
        $order_direction = $request->get('order_direction');

        /*$search_data = [];

        if($search && $search != "") {
            $search = explode(' ', $search);
            foreach ($search as $search_item) {
                $search_data[] = ['name', 'LIKE', '%'.($search_item).'%'];
                $search_data[] = ['surname', 'LIKE', '%'.($search_item).'%'];
            }
        }*/

        $items = Workers::where('id','>','0');

        if($order)
            $items->orderBy($order, $order_direction);

        if($search) {
            $search = explode(' ', $search);
            $items->where(function ($q) use ($search) {
                foreach ($search as $value) {
                    $q->orWhere('name', 'like', "%{$value}%");
                    $q->orWhere('surname', 'like', "%{$value}%");
                }
            });
        }

        $items = $items->paginate(20);

        if(!Auth::user()->can('manage.chef')) {
            $data = $items->getCollection();
            $data->each(function ($item) {
                $item->setHidden(['for_an_hour']); //->setVisible(['for_an_hour']);
            });
            $items->setCollection($data);
        }

        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items
        ];


        return response()->json($response);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'for_an_hour' => 'required',
            'company_id' => 'required'
        ]);

        $create = Workers::create($request->all());

        return response()->json($create);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Workers  $workers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'for_an_hour' => 'required',
            'company_id' => 'required'
        ]);

        $edit = Workers::find($id)->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Workers::find($id)->delete();
        return response()->json(['done']);
    }
}
