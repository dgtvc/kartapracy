<?php

namespace App\Http\Controllers;

use App\Models\BM;
use App\Models\Building;
use App\Models\Manager;
use Illuminate\Http\Request;

class BuildingController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manage() {
        return view('pages.buildings');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $nolimit = $request->exists('no_limit');
        $order = $request->get('order');
        $order_direction = $request->get('order_direction');
        $minonemanager = $request->exists('minonemanager');


        $items  = Building::where('id','>',0);

        if($order)
            $items->orderBy($order, $order_direction);

        if($search) {
            $search = explode(' ', $search);
            $items->where(function ($q) use ($search) {
                foreach ($search as $value) {
                    $q->orWhere('name', 'like', "%{$value}%");
                    $q->orWhere('contract_name', 'like', "%{$value}%");
                }
            });
        }

        $items->with('managers');

        if($minonemanager)
            $items->has('managers');

        if($nolimit)
            return response()->json($items->get());

        $items = $items->paginate(20);

        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items
        ];


        return response()->json($response);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'contract_name' => 'required',
        ]);

        if($request->get('manager_id') != 0) {
            $manager = Manager::find($request->get('manager_id'));

            if ($manager->mpk_own != null)
                return response()->json(array('success' => false, 'message' => 'Kierownik jest już przypisany jako główny kierownik MPK ' . $manager->mpk_own->name), 422);

        }

        $create = Building::create($request->all());

        return response()->json($create);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Building  $workers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'contract_name' => 'required',
        ]);

        $building = Building::find($id);

        if($request->get('manager_id') != 0) {
            $manager = Manager::find($request->get('manager_id'));

            if ($manager->mpk_own != null && $manager->mpk_own->id != $id)
                return response()->json(array('success' => false, 'message' => 'Kierownik jest już przypisany jako główny kierownik MPK ' . $manager->mpk_own->name), 422);

            if($manager->building_id != 0 && $manager->building_id != $id)
                return response()->json(array('success' => false, 'message' => 'Kierownik jest już przypisany jako kierownik MPK ' . $manager->building->name), 422);
        }

        $edit = $building->update($request->all());

        return response()->json($edit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Building::find($id)->delete();
        return response()->json(['done']);
    }

    public function setManager(Request $request, $manager, $value, $delete) {
        $manager = Manager::find($manager);
        if($manager) {
            $pow = BM::where([
                ['manager_id', '=', $manager->id],
                ['building_id', '=', $value]
            ])->first();

            if($pow && !$delete)
                return response()->json(['success' => false, 'message' => 'Kierownik jest już przypisany do MPK']);

            if($delete) {
                $pow->delete();
                return response()->json(['success' => true]);
            } else {
                $pow = new BM();
                $pow->manager_id = $manager->id;
                $pow->building_id = $value;
                $pow->save();
                return response()->json(['success' => true]);
            }
        }
        return response()->json(['success' => false, 'message' => 'Nie odnaleziono kierownika']);
    }
}
