<?php

namespace App\Http\Controllers;

use App\Export\ReportExport;
use App\Exports\ByMpkExport;
use App\Exports\ManagerExport;
use App\Exports\MpkOnlyExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{

    private $request;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.report');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|void
     */
    public function store(Request $request)
    {

        if (!empty($request)) {
            switch ($request->get('type')) {
                case 'mpk':
                    $this->validate($request, [
                        'month' => 'required',
                        'year' => 'required'
                    ]);

                    $export = new ByMpkExport($request->get('year'), $request->get('month'), $request);

                    $name = 'Raport dla danego MPK miesięcznie ' . $request->year . ' - ' . $request->month . '.xlsx';

                    return Excel::download($export, $name);

                    break;
                case 'mpkonly':
                    $this->validate($request, [
                        'month' => 'required',
                        'year' => 'required'
                    ]);

                    $export = new MpkOnlyExport($request->get('year'), $request->get('month'), $request);

                    $name = 'Raport dla danego MPK ' . $request->year . ' - ' . $request->month . '.xlsx';

                    return Excel::download($export, $name);

                    break;
                case 'manager':
                    $this->validate($request, [
                        'month' => 'required',
                        'year' => 'required'
                    ]);

                    $export = new ManagerExport($request->get('year'), $request->get('month'), $request);

                    $name = 'Raport danego kierownika' . $request->year . ' - ' . $request->month . '.xlsx';

                    return Excel::download($export, $name);

                    break;

                default:
                    $this->validate($request, [
                        'date_from' => 'required',
                        'date_to' => 'required'
                    ]);

                    $export = new ReportExport($request->date_from, $request->date_to, $request); //->view()); //, 'Raport ' . $request->date_from . ' - ' . $request->date_to . '.xlsx');

                    $name = 'Raport ogólny' . $request->date_from . ' - ' . $request->date_to . '.xlsx';

                    return Excel::download($export, $name);
                break;
            }
        }
    }
}
