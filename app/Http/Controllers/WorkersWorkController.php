<?php

namespace App\Http\Controllers;

use App\Models\Workers;
use App\Models\WorkersWork;
use App\Models\WorkersWorkChanges;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class WorkersWorkController
 * @package App\Http\Controllers
 */
class WorkersWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        DB::enableQueryLog();

        $search = $request->get('search');
        $name_search = $request->get('name_search');
        $order = $request->get('order');
        $order_direction = $request->get('order_direction');


        $items = WorkersWork::query();

        if($order)
            $items->orderBy($order, $order_direction);


        if($search) {
            $search = explode(' ', $search);
            $items->where(function ($q) use ($search) {
                foreach ($search as $value) {
                    $q->where('date', 'like', "%{$value}%");
                    $q->orWhere('desc', 'like', "%{$value}%");
                }
            });
        }

        $name_search_array = [];
        if($name_search) {
            $name_search_array = explode(' ', $name_search);
        }

        $items
            ->with(['user' => function($query) use ($name_search_array){
                if($name_search_array) {
                    foreach ($name_search_array as $value) {
                        $query->where('name', 'like', "%{$value}%");
                        $query->orWhere('surname', 'like', "%{$value}%");
                    }
                }
            }])
            ->with(array('changes' => function($query) {
                $query->with('user');
                $query->orderBy('created_at','desc');
            }))
            ->with(array('building' => function($query) {

            }))
            ->with(array('manager' => function($query) {

            }));

        if($name_search) {
            $items->whereHas('user',function ($q) use ($name_search_array) {
                foreach ($name_search_array as $value) {
                    $q->where('name', 'like', "%{$value}%");
                    $q->orWhere('surname', 'like', "%{$value}%");
                }
                return $q;
            });
        }


        $items = $items->paginate(20);

        if(!Auth::user()->can('manage.chef')) {
            $data = $items->getCollection();
            $data->each(function ($item) {
                $item->setHidden(['per_hour']);
            });
            $items->setCollection($data);
        }

        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'chef' => Auth::user()->can('manage.chef'),
            'data' => $items
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'building_id' => 'required|integer',
                'date' => 'required|date|date_format:Y-m-d',
                'desc' => 'required|string',
                'manager_id' => 'required|integer',
                'time_from' => 'required|string',
                'time_to' => 'required|string',
                'users' => 'required|array|min:1'
            ]);

            $desc = $request->get('desc');
            $date = $request->get('date');
            $manager_id = $request->get('manager_id');
            $building_id = $request->get('building_id');

            $time_from = $request->get('time_from');
            $time_to = $request->get('time_to');

            $time_count = ((int)$time_to) - ((int)$time_from);

            $time_from = $time_from . ':00';
            $time_to = $time_to . ':00';

            foreach ($request->get('users') as $user_id) {
                $user = Workers::find($user_id);
                $work = new WorkersWork();
                $work->worker_id = $user_id;
                $work->manager_id = $manager_id;
                $work->building_id = $building_id;
                $work->desc = $desc;
                $work->date = $date;
                $work->time_from = $time_from;
                $work->time_to = $time_to;
                $work->time_count = $time_count;
                $work->per_hour = $user->for_an_hour;
                $work->save();
            }
        } catch(Exception $exception) {
            dd($exception);
            die();
        }

        return response()->json(['success' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param WorkersWork $workersWork
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkersWork $workersWork)
    {
        return view('pages.timemanage');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param WorkersWork $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $work = WorkersWork::find($id);

        $this->validate($request, [
            'date' => 'required|date|date_format:Y-m-d',
            'desc' => 'required|string',
            'time_from' => 'required|string',
            'time_to' => 'required|string',
            'building' => 'required|integer',
            'manager' => 'required|integer',
        ]);

        $time_from = $request->get('time_from');
        $time_to = $request->get('time_to');

        $time_count = ((int) $time_to) - ((int) $time_from);

        $time_from = $time_from.':00';
        $time_to = $time_to.':00';

        $work->desc = $request->get('desc');
        $work->date = $request->get('date');
        $work->time_from = $time_from;
        $work->time_to = $time_to;
        $work->time_count = $time_count;

        $work->manager_id = $request->get('manager');
        $work->building_id = $request->get('building');

        $work->save();

        $user = Auth::user();

        $update = WorkersWorkChanges::create([
            'user_id' => $user->id,
            'workers_work_id' => $id
        ]);

        return response()->json($work);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param WorkersWork $workersWork
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $workersWork = WorkersWork::find($id);
        $workersWork->delete();
        return response()->json(['done']);
    }
    public function delete( $id)
    {
        $workersWork = WorkersWork::find($id);
        $workersWork->delete();
        return response()->json(['done']);
    }
}
