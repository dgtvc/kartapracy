<?php
/**
 * Created by PhpStorm.
 * User: dgtvc
 * Date: 06.08.2018
 * Time: 01:01
 */

namespace App\Http\Controllers;


class HomeController extends Controller
{

    public function index() {
        return view('pages.index');
    }

}