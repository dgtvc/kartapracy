## About project

Workers work card

- Manage users and workers
- Manage work time cards
- Export work cards to salary per month

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
