<div class="page aside-left">
    <div class="page-main">
        <div class="header header-top py-4">
            <div class="container">
                <div class="d-flex">
                    <div class="header-brand" href="{{ route('home') }}">
                        <img src="/images/logo.png?v1">
                    </div>
                    <div class="d-flex order-lg-2 ml-auto">
                        <div class="dropdown">
                            <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                                <div class="avatar bg-yellow-dark">
                                    <i class="fe h1 fe-user text-white"></i>
                                </div>
                                <span class="ml-2 d-none d-lg-block">
                                  <b class="text-default">{{ Auth::user()->name }}</b>
                                  <small class="text-muted d-block mt-1">{{ lcfirst(Auth::user()->getRoleNames()->implode(',')) }}</small>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="#">
                                    <i class="dropdown-icon fe fe-settings"></i> Ustawienia
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}">
                                    <i class="dropdown-icon fe fe-log-out"></i> Wyloguj się
                                </a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
                       data-target="#headerMenuCollapse">
                        <span class="header-toggler-icon"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
            <div class="container">
                <div class="row align-items-center">
                    <!--<div class="col-lg-3 ml-auto">
                        <form class="input-icon my-3 my-lg-0">
                            <input type="search" class="form-control header-search" placeholder="Search&hellip;"
                                   tabindex="1">
                            <div class="input-icon-addon">
                                <i class="fe fe-search"></i>
                            </div>
                        </form>
                    </div>-->
                    <div class="col-lg order-lg-first">
                        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                            <li class="nav-item">
                                <a href="{{ route('home') }}" class="nav-link {{ Menu::activeMenu('home') }}"><i class="fe fe-home"></i>
                                    Strona główna</a>
                            </li>
                            @if(auth()->user()->hasAnyPermission(['manage.workers','manage.time']))

                            @endif
                            @can('manage.time')
                                <li class="nav-item dropdown">
                                    <a href="javascript:void(0)" class="nav-link {{ Menu::activeMenu('workers/time') }}{{ Menu::activeMenu('workers/time/edit') }}" data-toggle="dropdown"><i
                                                class="fe fe-user"></i> Godziny pracy</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        @can('manage.time')
                                            <a href="{{ route('workers.time') }}" class="dropdown-item ">Dodaj</a>
                                            <a href="{{ route('workers.time.edit') }}" class="dropdown-item ">Edytuj</a>
                                        @endcan
                                    </div>
                                </li>
                            @endcan
                            @can('manage.workers')
                            <li class="nav-item dropdown">
                                <a href="{{ route('workers.manage') }}" class="nav-link {{ Menu::activeMenu('workers/manage') }}">
                                    <i class="fe fe-user"></i> Pracownicy
                                </a>
                            </li>
                            @endcan
                            @can('manage.workers')
                                <li class="nav-item">
                                    <a href="{{ route('manger.manage') }}" class="nav-link {{ Menu::activeMenu('manager/manage') }}">
                                        <i class="fe fe-crosshair"></i> Kierownicy
                                    </a>
                                </li>
                            @endcan
                            @can('manage.buildings')
                            <li class="nav-item">
                                <a href="{{ route('mpk.manage') }}" class="nav-link {{ Menu::activeMenu('mpk/manage') }}">
                                    <i class="fe fe-box"></i> MPK
                                </a>
                            </li>
                            @endcan
                            @can('manage.chef')
                            <li class="nav-item">
                                <a href="{{ route('users.manage') }}" class="nav-link {{ Menu::activeMenu('users/manage') }}">
                                    <i class="fe fe-user"></i> Użytkownicy
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('company.manage') }}" class="nav-link {{ Menu::activeMenu('company/manage') }}">
                                    <i class="fe fe-globe"></i> Firmy
                                </a>
                            </li>
                            @endcan
                            @can('raport.generate')
                            <li class="nav-item">
                                <a href="{{ route('report.index') }}" class="nav-link {{ Menu::activeMenu('report') }}">
                                    <i class="fe fe-file"></i> Raporty
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="my-3 my-md-5">
            <div id="app">
                <div class="container">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
                    Copyright © 2018 All rights reserved.
                </div>
            </div>
        </div>
    </footer>
</div>