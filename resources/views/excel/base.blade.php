<table>
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Pracownik
            </th>
            <th>
                Budowa
            </th>
            <th>
                Opis
            </th>
            <th>
                Data
            </th>
            <th>
                Od
            </th>
            <th>
                Do
            </th>
            <th>
                Liczba godzin
            </th>
            <th>
                Na godzinę
            </th>
            <th>
                Suma(na pracę)
            </th>
            <th>
                Łącznie
            </th>
        </tr>
    </thead>
    <tbody>
        @php
            $id = 1;
            $last_id = 0;
            $last_sum = 0;
            $total = 0;
        @endphp
        @foreach($work_list as $work)
        <tr>
            <td>
                {{ $id++ }}
            </td>
            <td>
                {{ $last_id != $work->user->id? $work->user->name.' '.$work->user->surname:'' }}
                @php($last_id = $work->user->id)
            </td>
            <td>
                {{ $work->building->name }}
            </td>
            <td>
                {{ $work->desc  }}
            </td>
            <td>
                {{ $work->date }}
            </td>
            <td>
                {{ $work->time_from }}
            </td>
            <td>
                {{ $work->time_to }}
            </td>
            <td>
                {{ $work->time_count }}
            </td>
            <td>
                {{ $work->per_hour }}
            </td>
            <td>
                {{ $work->time_count * $work->per_hour }}
            </td>
            <td>

            </td>
        </tr>
        @endforeach
        <th rowspan="" align="right">

        </th>
    </tbody>
</table>