<table style="width: 100%">
    <thead>
    <tr>
        <th>
            MPK
        </th>
        <th>
            Kierownik
        </th>
        <th>
            Suma godzin na danego kierownika
        </th>
    </tr>
    </thead>
    <tbody>
    @php
        $last_id = 0;
        $last_sum = 0;
        $total = 0;
        $hour = 0;
        $id = 0;

        $managers = [];
    @endphp
    @foreach($user_list as $user)
        @php
            $id++;
            $id_local = 1;
            $last_sum = 0;
            $last_hour = 0;
            $first = false;

            $params = [];

            $date_from = $year.'-'.(($month < 10? "0".$month:$month)).'-'.'01';
            $date_to = $year.'-'.(($month < 10? "0".$month:$month)).'-'.'31';

            $params[] = ['date', '>=', $date_from];
            $params[] = ['date', '<=', $date_to];
            $params[] = ['worker_id', '=' , $user->id];

            $work_list = \App\Models\WorkersWork::where($params)->orderBy('manager_id')->orderBy('worker_id')->get();

            $count = $work_list->count();
        @endphp
        @if($work_list->count())

            @php
                foreach ($work_list as $work) {

                    $manager = new stdClass();

                    $manager_name = sprintf('%s %s / %s', $work->managerr->name, $work->managerr->surname, $work->managerr->city);

                    if(isset($managers[$manager_name])) {

                        $manager = $managers[$manager_name];

                    } else {

                        $manager->data = [];
                        $manager->total = 0;

                    }

                    $manager->total += $work->time_count;

                    $building = 0;

                    if(isset($manager->data[$work->buildingg->name]))
                        $building = $manager->data[$work->buildingg->name];

                    $manager->data[$work->buildingg->name] = $building + $work->time_count;

                    $managers[$manager_name] = $manager;
                }

            @endphp
        @endif
    @endforeach

    @foreach($managers as $name => $manager)
        @foreach($manager->data as $datum => $val)
            <tr>
                <td>
                    {{ $datum }}
                </td>
                <td>
                    {{ $name }}
                </td>
                <td>
                    {{ $val }}
                </td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>