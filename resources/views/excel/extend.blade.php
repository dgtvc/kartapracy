<table style="width: 100%">
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Pracownik
        </th>
        <th>
            MPK
        </th>
        <th>
            Firma
        </th>
        <th>
            Kierownik
        </th>
        <th>
            Opis
        </th>
        <th>
            Data
        </th>
        <th>
            Od
        </th>
        <th>
            Do
        </th>
        <th>
            Liczba godzin
        </th>
        <th>
            Na godzinę
        </th>
        <th>
            Suma(na pracę)
        </th>
        <th>
            Łącznie
        </th>
    </tr>
    </thead>
    <tbody>
    @php
        $last_id = 0;
        $last_sum = 0;
        $total = 0;
        $hour = 0;
        $id = 0;
    @endphp
    @foreach($user_list as $user)
        @php
            $id++;
            $id_local = 1;
            $last_sum = 0;
            $last_hour = 0;
            $first = false;

            $params = [];

            $params[] = ['date', '>=', $date_from];
            $params[] = ['date', '<=', $date_to];
            $params[] = ['worker_id','=',$user->id];

            // Po pracowniku

            if($request->has('worker'))
                $params[] = ['worker_id','=',$request->get('worker')];

            // Po MPK

            if($request->has('mpk'))
                $params[] = ['building_id','=',$request->get('mpk')];

            // Kierownik

            if($request->has('manager'))
                $params[] = ['manager_id','=',$request->get('manager')];

            $work_list = \App\Models\WorkersWork::where($params)->orderBy('date')->get();

            $count = $work_list->count();
        @endphp
        @if($work_list->count())
        @foreach($work_list as $work)
            <tr>
                <td>
                    {{ $id }} - {{ $id_local++ }}
                </td>
                @if(!$first)
                    @php($first = true)
                    <td rowspan="{{ $count }}">
                        {{ $user->name.' '.$user->surname }} / {{ $user->city }},  {{ $user->street }}
                    </td>
                @endif
                <td>
                    {{ $work->buildingg->name }}
                </td>
                <td>
                    {{ $user->company->name }}
                </td>
                <td>
                    {{ $work->managerr->name }}
                    {{ $work->managerr->surname }}
                    /
                    {{ $work->managerr->city }}
                </td>
                <td>
                    {{ $work->desc  }}
                </td>
                <td>
                    {{ $work->date }}
                </td>
                <td>
                    {{ $work->time_from }}
                </td>
                <td>
                    {{ $work->time_to }}
                </td>
                <td>
                    @php($last_hour += $work->time_count)
                    {{ $work->time_count }}
                </td>
                <td>
                    {{ $work->per_hour }}
                </td>
                <td>
                    @php($last_sum += $work->time_count * $work->per_hour)
                    {{ $work->time_count * $work->per_hour }}
                </td>
                <td>

                </td>
            </tr>
        @endforeach
        @php($total += $last_sum)
        @php($hour += $last_hour)

        <tr>
            <td>
                {{ $id }} - Podsumowanie pracownika
            </td>
            <td colspan="2">

            </td>
            <td> {{ $user->company->name }} </td>
            <td colspan="4">

            </td>
            <td>
                {{ $last_hour }}
            </td>
            <td colspan="2">

            </td>
            <td>
                {{ $last_sum }}
            </td>
            <td>

            </td>
        </tr>
        @endif
    @endforeach

    <tr>
        <td>
            Podsumowanie
        </td>
        <td colspan="7">

        </td>
        <td>
            {{ $hour }}
        </td>
        <td colspan="2">

        </td>
        <td>
            {{ $total }}
        </td>
    </tr>
    </tbody>
</table>