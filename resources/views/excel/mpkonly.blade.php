<table style="width: 100%">
    <thead>
    <tr>
        <th>
            MPK
        </th>
        <th>
            suma godzin na danym mpk
        </th>
    </tr>
    </thead>
    <tbody>
    @php
        $last_id = 0;
        $last_sum = 0;
        $total = 0;
        $hour = 0;
        $id = 0;
        $data = [];
    @endphp
    @foreach($user_list as $user)
        @php
            $id++;
            $id_local = 1;
            $last_sum = 0;
            $last_hour = 0;
            $first = false;

            $params = [];

            $date_from = $year.'-'.(($month < 10? "0".$month:$month)).'-'.'01';
            $date_to = $year.'-'.(($month < 10? "0".$month:$month)).'-'.'31';

            $params[] = ['date', '>=', $date_from];
            $params[] = ['date', '<=', $date_to];
            $params[] = ['worker_id', '=' , $user->id];

            $work_list = \App\Models\WorkersWork::where($params)->orderBy('building_id')->orderBy('worker_id')->get();

            $count = $work_list->count();

        @endphp
        @if($work_list->count())

            @php
                foreach ($work_list as $work) {

                    $arr = new stdClass();

                    $arr->building = '';
                    $arr->total = 0;


                    if(isset($data[$work->buildingg->name]))
                        $arr = $data[$work->buildingg->name];
                    else {
                        $arr->building = $work->buildingg->name;
                    }

                    $arr->total = $arr->total + $work->time_count;

                    $data[$work->buildingg->name] = $arr;
                }

            @endphp
        @endif
    @endforeach

    @foreach($data as $work)
        <tr>
            <td>
                {{ $work->building }}
            </td>
            <td>
                {{ $work->total }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>