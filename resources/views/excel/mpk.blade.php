<table style="width: 100%">
    <thead>
    <tr>
        <th>
            Miesiąc
        </th>
        <th>
            MPK
        </th>
        <th>
            Pracownik
        </th>
        <th>
            Kierownik
        </th>
        <th>
            Suma godzin na danym mpk w podsumowaniu miesiecznym
        </th>
    </tr>
    </thead>
    <tbody>
    @php
        $last_id = 0;
        $last_sum = 0;
        $total = 0;
        $hour = 0;
        $id = 0;
    @endphp
    @foreach($user_list as $user)
        @php
            $id++;
            $id_local = 1;
            $last_sum = 0;
            $last_hour = 0;
            $first = false;

            $params = [];

            $date_from = $year.'-'.(($month < 10? "0".$month:$month)).'-'.'01';
            $date_to = $year.'-'.(($month < 10? "0".$month:$month)).'-'.'31';

            $params[] = ['date', '>=', $date_from];
            $params[] = ['date', '<=', $date_to];
            $params[] = ['worker_id', '=' , $user->id];

            $work_list = \App\Models\WorkersWork::where($params)->orderBy('building_id')->orderBy('worker_id')->get();

            $count = $work_list->count();
        @endphp
        @if($work_list->count())

            @php

                $data = [];

                foreach ($work_list as $work) {

                    $arr = new stdClass();

                    $arr->building = '';
                    $arr->worker = '';
                    $arr->manager = '';
                    $arr->total = 0;


                    if(isset($data[$work->buildingg->name]))
                        $arr = $data[$work->buildingg->name];
                    else {

                        $arr->building = $work->buildingg->name;
                        $arr->worker = sprintf('%s %s / %s %s', $user->name, $user->surname, $user->city, $user->street);
                        $arr->manager = sprintf('%s %s / %s', $work->managerr->name, $work->managerr->surname, $work->managerr->city);
                    }

                    $arr->total = $arr->total + $work->time_count;

                    $data[$work->building->name] = $arr;
                }

            @endphp

            @foreach($data as $work)
                <tr>
                    <td>
                        {{ $month }} / {{ $year }}
                    </td>
                    <td>
                        {{ $work->building }}
                    </td>
                    <td>
                        {{ $work->worker }}
                    </td>
                    <td>
                        {{ $work->manager }}
                    </td>
                    <td>
                        {{ $work->total }}
                    </td>
                </tr>
            @endforeach
        @endif
    @endforeach
    </tbody>
</table>