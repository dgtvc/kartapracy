@extends('layouts.app')

@section('content')

    <div class="page">
        <div class="page-single">
            <div class="container">
                <div class="row">
                    <div class="col col-login mx-auto">
                        <div class="text-center mb-6">
                            <img src="./assets/brand/tabler.svg" class="h-6" alt="">
                        </div>
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class="card">
                            {{ csrf_field() }}
                            <div class="card-body p-6">
                                <div class="card-title">Zaloguj się do swojego konta</div>
                                <div class="form-group">
                                    <label class="form-label">Adres email</label>
                                    <input id="email" placeholder="Wpis swój adres email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label">
                                        Hasło
                                        <a href="{{ route('password.request') }}" class="float-right small">Zapomniałem hasła</a>
                                    </label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Wpisz hasło" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <span class="custom-control-label">Zapamiętaj mnie</span>
                                    </label>
                                </div>
                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Zaloguj się</button>
                                </div>
                            </div>
                        </form>
                        <!--<div class="text-center text-muted">
                            Don't have account yet? <a href="./register.html">Sign up</a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
