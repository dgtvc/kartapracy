@extends('layouts.app')

@section('content')

    <div class="page-header">
        <h1 class="page-title">
            Uprawnienia
        </h1>
    </div>
    <div class="card card-body">
        <table class="table table-responsive">
            <thead>
            <tr>
                <th>
                    Rola
                </th>
                @foreach($permissions as $permission)
                    <th>
                        {{ $permission->full_name }}
                    </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>
                        {{ $role->full_name }}
                    </td>
                    @foreach($permissions as $permission)
                        <td>
                            {{ $role->hasPermissionTo($permission)? '+':'-'}}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection