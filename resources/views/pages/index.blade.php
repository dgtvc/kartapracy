@extends('layouts.app')

@section('content')
                <div class="container">
                    <div class="page-header">
                        <h1 class="page-title">
                            Witaj w naszym serwisie
                        </h1>
                    </div>
                    <div class="row">
                        <div class="card">
                            <div class="card-header">
                                <h6> Nowa przyszłość zarządzania pracownikami </h6>
                            </div>
                            <div class="card-body">
                                <p>
                                    Dzięki naszemu systemowi pomożemy usprawnić Twoje zarządzania pracownikami w firmię.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
