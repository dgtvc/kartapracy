
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.toastr = require('toastr');
window.swal = require('sweetalert2');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding views to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./views/ExampleComponent.vue'));
Vue.component('workers', require('./views/WorkersComponent'));
Vue.component('managers', require('./views/MangerComponent'));
Vue.component('buildings', require('./views/BuildingsComponent'));
Vue.component('chef', require('./views/ChefComponent'));
Vue.component('timemanage', require('./views/TimeComponent'));
Vue.component('timemanager', require('./views/TimeManageComponent'));
Vue.component('report', require('./views/ReportComponent'));
Vue.component('company', require('./views/CompanyComponent'));

const app = new Vue({
    el: '#app'
});
