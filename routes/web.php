<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Config::set('debugbar.enabled', true);

Auth::routes();

// Zalogowany

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'UserController@index')->name('home');
    Route::get('/home', 'UserController@index')->name('home');

    // Pracownicy

    // Czas

        // Dodawanie prac

        Route::get('workers/time', 'WorkersController@time')->name('workers.time');
        Route::post('workers/time/add', 'WorkersWorkController@store')->name('workers.time.add');

        // Listowanie prac

        Route::get('workers/time/edit', 'WorkersWorkController@edit')->name('workers.time.edit');
        Route::get('workers/time/list', 'WorkersWorkController@index')->name('workers.time.list');
        Route::delete('workers/time/delete/{id}', 'WorkersWorkController@delete')->name('workers.time.delete');
        Route::post('workers/time/update/{id}', 'WorkersWorkController@update')->name('workers.time.update');
        Route::put('workers/time/update/{id}', 'WorkersWorkController@update')->name('workers.time.update');


        // Ludzie
        Route::get('workers/manage', 'WorkersController@manage')->name('workers.manage');
        Route::resource('workers','WorkersController');

    // Kierownicy
    Route::get('manger/manage', 'ManagerController@manage')->name('manger.manage');
    Route::resource('manger','ManagerController');

    // Budowle
    Route::get('mpk/manage', 'BuildingController@manage')->name('mpk.manage');
    Route::post('mpk/setmanager/{manager}/{value}/{delete?}', 'BuildingController@setManager')->name('mpk.manager');
    Route::resource('mpk','BuildingController');

    // Użytkownicy
    Route::get('users/manage', 'ChefController@manage')->name('users.manage');
    Route::get('users/permissions', 'ChefController@showPermissions')->name('users.permissions');
    Route::resource('users','ChefController');

    // Firmy
    Route::get('company/manage', 'CompanyController@manage')->name('company.manage');
    Route::resource('company','CompanyController');

    // Raporty

    Route::resource('report', 'ReportController');

    // Wylogowanie
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
});

// Guest

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', 'HomeController@index')->name('home');
});